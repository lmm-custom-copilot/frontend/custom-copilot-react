import { useRef, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import Editor from '@monaco-editor/react'
import './App.css'

function App() {
  const [count, setCount] = useState(0)
  const editorRef = useRef(null)

  function handleEditorDidMount(editor, monaco){
    editorRef.current = editor
  }

  function showValue(){
    alert(editorRef.current.getValue())
  }

  return (
    <Editor height="90vh" width="90vh" defaultLanguage='python' defaultValue='#komentarz'></Editor>
  )
}

export default App
